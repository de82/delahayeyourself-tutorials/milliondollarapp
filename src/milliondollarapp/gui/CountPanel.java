/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package milliondollarapp.gui;

import javax.swing.JOptionPane;

/**
 *
 * @author sam
 */
public class CountPanel extends javax.swing.JPanel {

    int total = 0;
    /**
     * Creates new form CountPanel
     */
    public CountPanel() {
        initComponents();
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel1 = new javax.swing.JLabel();
        totalLabel = new javax.swing.JLabel();
        clickMeButton = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        quoteTextField = new javax.swing.JTextField();
        greetMeButton = new javax.swing.JButton();

        jLabel1.setText("Total:");

        totalLabel.setText("0");

        clickMeButton.setText("Click me!");
        clickMeButton.setToolTipText("");
        clickMeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clickMeButtonActionPerformed(evt);
            }
        });

        jLabel2.setText("Quote:");

        quoteTextField.setToolTipText("");

        greetMeButton.setText("Greet Me!");
        greetMeButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                greetMeButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(58, 58, 58)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jLabel2)
                    .addComponent(jLabel1))
                .addGap(53, 53, 53)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(totalLabel)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(clickMeButton)
                        .addGap(21, 21, 21))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(quoteTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 113, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(greetMeButton)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel1)
                    .addComponent(totalLabel)
                    .addComponent(clickMeButton))
                .addGap(21, 21, 21)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel2)
                    .addComponent(quoteTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(greetMeButton))
                .addContainerGap(202, Short.MAX_VALUE))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void clickMeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clickMeButtonActionPerformed
        this.total += 1;
        this.totalLabel.setText(String.format("%s", this.total));
    }//GEN-LAST:event_clickMeButtonActionPerformed

    private void greetMeButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_greetMeButtonActionPerformed
        if(this.quoteTextField.getText().isEmpty()){
            JOptionPane.showMessageDialog(this, "Please fill form", "Warning", JOptionPane.WARNING_MESSAGE);
        }else{
            JOptionPane.showMessageDialog(this, this.quoteTextField.getText(), "Greeting ..", JOptionPane.INFORMATION_MESSAGE);
        }
        
    }//GEN-LAST:event_greetMeButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton clickMeButton;
    private javax.swing.JButton greetMeButton;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JTextField quoteTextField;
    private javax.swing.JLabel totalLabel;
    // End of variables declaration//GEN-END:variables
}
